#
# This file is part of LiteX-Boards.
#
# Copyright (c) 2017 Sergiusz Bazanski <q3k@q3k.org>
# Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2020-2021 Raptor Engineering, LLC
# SPDX-License-Identifier: BSD-2-Clause

from litex.build.generic_platform import *
from litex.build.lattice import LatticePlatform
from litex.build.lattice.programmer import OpenOCDJTAGProgrammer

# IOs ----------------------------------------------------------------------------------------------

_io = [
    # Clk / Rst
    ("clk24", 0, Pins("B6"), IOStandard("LVCMOS33")),
    ("rst_n",  0, Pins("T2"), IOStandard("LVCMOS33")),	# Front panel power button, for easy debugging

    # GPIO Bank 1
    # BMC_POWER_UP | BMC_BOOT_PHASE | BP_SYSRESET_N | DBG_FSI0_EN | SYS_PWROK_BUF | BMC_INTRUDER_N | PM_CP0_ATTENTION_B | BMC_FP_ID_BTN
    ("gpio1", 0, Pins("T17 U16 C5 C6 T1 V1 R3 U2"), IOStandard("LVCMOS33")),

    # Serial (BMC COM1)
    ("serial", 0,
        Subsignal("rx", Pins("D10"), IOStandard("LVCMOS33")),
        Subsignal("tx", Pins("E10"), IOStandard("LVCMOS33")),
    ),

    # NCSI Ethernet
    ("eth_clocks", 0,
        Subsignal("ref_clk", Pins("A9")),
        IOStandard("LVCMOS33")
    ),
    ("eth", 0,
        Subsignal("crs_dv",  Pins("B9")),
        Subsignal("rx_data", Pins("A10 B10")),
        Subsignal("tx_en",   Pins("B11")),
        Subsignal("tx_data", Pins("C11 A11")),
        IOStandard("LVCMOS33")
    ),

    # PCIe
    ("pcie_x1", 0,
        Subsignal("clk_p", Pins("Y11")),
        Subsignal("clk_n", Pins("Y12")),
        Subsignal("rx_p",  Pins("Y5")),
        Subsignal("rx_n",  Pins("Y6")),
        Subsignal("tx_p",  Pins("W4")),
        Subsignal("tx_n",  Pins("W5")),
        Subsignal("perst", Pins("A6"), IOStandard("LVCMOS33")),
    ),

    # I2C
    # Regulators
    ("i2c_master", 1,
        Subsignal("sda",            Pins("B8"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
        Subsignal("scl",            Pins("A7"),  IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
    ),

    # SEEPROM
    ("i2c_master", 2,
        Subsignal("sda",            Pins("D9"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
        Subsignal("scl",            Pins("A8"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
    ),

    # CPU clock
    ("i2c_master", 3,
        Subsignal("sda",            Pins("U1"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
        Subsignal("scl",            Pins("R1"),  IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
    ),

    # Platform control FPGA / temperature sensors
    ("i2c_master", 4,
        Subsignal("sda",            Pins("C7"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
        Subsignal("scl",            Pins("E8"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
    ),

    # RTC
    ("i2c_master", 5,
        Subsignal("sda",            Pins("C8"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
        Subsignal("scl",            Pins("D8"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
    ),

    # Digital Video
    ("i2c_master", 6,
        Subsignal("sda",            Pins("L17"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
        Subsignal("scl",            Pins("L18"), IOStandard("LVCMOS33"), Misc("PULLMODE=UP")),
    ),

    # FSI
    ("openfsi_master", 0,
        Subsignal("clock",          Pins("E7"), IOStandard("LVCMOS33")),
        Subsignal("data",           Pins("D6"), IOStandard("LVCMOS33")),
        Subsignal("data_direction", Pins("D7"), IOStandard("LVCMOS33")),
    ),

    # ECP5 Flash device
    # Also contains FPGA bistream, USRMCLK block required for clock output
    ("bmcspiflash4x", 0,
        Subsignal("cs_n", Pins("R2")),
        Subsignal("dq",   Pins("W2 V2 Y2 W1")),
        IOStandard("LVCMOS33"),
        Misc("SLEWRATE=SLOW"),
        Misc("DRIVE=16"),
    ),

    # Host Flash device
    ("hostspiflash4x", 0,
        Subsignal("cs_n", Pins("E2")),
        Subsignal("clk",  Pins("G3")),
        Subsignal("dq",   Pins("F2 F3 B5 B2")),
        IOStandard("LVCMOS33"),
        Misc("SLEWRATE=SLOW"),
        Misc("DRIVE=16"),
    ),

    # Host LPC interface
    ("hostlpcslave", 0,
        Subsignal("frame_n",   Pins("D3"), Misc("PULLMODE=UP")),
        Subsignal("reset_n",   Pins("C3"), Misc("PULLMODE=UP")),
        Subsignal("addrdata",  Pins("C4 A3 B4 B3"), Misc("PULLMODE=UP")),
        Subsignal("serirq",    Pins("F4"), Misc("PULLMODE=UP")),
        Subsignal("clk",       Pins("H5"), Misc("PULLMODE=NONE")),
        IOStandard("LVCMOS33"),
        Misc("SLEWRATE=SLOW"),
        Misc("DRIVE=16"),
    ),
    ("hostlpcsecureinterface", 0,
        Subsignal("clk_out",   Pins("H3"), Misc("PULLMODE=UP")),
        IOStandard("LVCMOS33"),
        Misc("SLEWRATE=FAST"),
        Misc("DRIVE=16"),
    ),

    # Digital video
    ("dvo", 0,
        Subsignal("r", Pins(
            "R16 P16 N17 P17 N18 M17 N16 M18")),
        Subsignal("g", Pins(
            "T20 R20 P20 P18 P19 N20 N19 R17")),
        Subsignal("b", Pins(
            "T16 R18 T19 U17 U18 T18 U19 U20")),
        Subsignal("de",      Pins("M19")),
        Subsignal("hsync_n", Pins("L19")),
        Subsignal("vsync_n", Pins("M20")),
        Subsignal("clk",     Pins("L20")),
        IOStandard("LVCMOS33")
    ),
]

# Connectors ---------------------------------------------------------------------------------------

_connectors = [
]

# Platform -----------------------------------------------------------------------------------------

class Platform(LatticePlatform):
    default_clk_name   = "clk24"
    default_clk_period = 1e9/24e6

    def __init__(self, device="LFE5UM5G", toolchain="trellis", **kwargs):
        assert device in ["LFE5UM5G", "LFE5UM"]
        LatticePlatform.__init__(self, device + "-85F-8BG381C", _io, _connectors, toolchain=toolchain, **kwargs)

    def create_programmer(self):
        return OpenOCDJTAGProgrammer("openocd_sparrowhawk_bmc.cfg")

    def do_finalize(self, fragment):
        self.add_period_constraint(self.lookup_request("clk24", loose=True), 1e9/24e6)
        self.add_period_constraint(self.lookup_request("eth_clocks:ref_clk", 0, loose=True), 1e9/50e6)
