#!/usr/bin/env python3

#
# This file is part of Kestrel
#
# Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# Copyright (c) 2020-2023 Raptor Engineering, LLC
# SPDX-License-Identifier: BSD-2-Clause

import os
import argparse
import subprocess
import tempfile

from migen import *
from migen.genlib.resetsync import AsyncResetSynchronizer

from litex_boards.platforms import rcs_arctic_tern_bmc_card

from litex.build.lattice.trellis import trellis_args, trellis_argdict

from litex.soc.cores.clock import *
from litex.soc.integration.soc import SoCRegion
from litex.soc.integration.soc_core import *
from litex.soc.integration.soc_sdram import *
from litex.soc.integration.builder import *
from litex.soc.interconnect.csr import AutoCSR
from litex.soc.interconnect.csr import CSRStorage
from litex.soc.interconnect.csr import CSRStatus
from litex.soc.cores.video import VideoDVOPHY
from litex.soc.cores.led import LedChaser
from litex.soc.cores.gpio import _GPIOIRQ

from litedram.modules import MT41J256M16
from litedram.phy import ECP5DDRPHY
from litedram.common import get_default_cl

from liteeth.phy.ecp5rgmii import LiteEthPHYRGMII

# Kestrel-specific peripherals
from tercelspi.tercelspi import TercelSPI
from tercelspi.tercelspi import ADS7950Driver
from aquilalpc.aquilalpc import AquilaLPCSlave
from swiftfsi.swiftfsi import OpenFSIMaster
from simplertc.simplertc import SimpleRTCSlave
from simplepwm.simplepwm import SimplePWMSlave
from opencoresi2c.opencoresi2c import OpenCoresI2CMaster
from opencores1wire.opencores1wire import OpenCores1WireMaster
from arcticterngpio.arcticterngpio import ArcticTernGPIOI2CExists
from arcticterngpio.arcticterngpio import ArcticTernSDRHexDriver

# Useful constants
kB = 1024
mB = 1024*kB

# Internal GPIO Output -----------------------------------------------------------------------------
class InternalGPIOOut(Module, AutoCSR):
    def __init__(self, data_out, nbits):
        self._out = CSRStorage(nbits, description="GPIO Ouptut(s) Control.")

        # # #

        for i in range(nbits):
            self.comb += data_out[i].eq(self._out.storage[i])

# Internal GPIO Tristate ---------------------------------------------------------------------------
class InternalGPIOTristate(_GPIOIRQ, Module, AutoCSR):
    def __init__(self, data_in, data_out, data_oe, nbits, with_irq=False):
        self._oe  = CSRStorage(nbits, description="GPIO Tristate(s) Control.")
        self._in  = CSRStatus(nbits,  description="GPIO Input(s) Status.")
        self._out = CSRStorage(nbits, description="GPIO Ouptut(s) Control.")

        # # #

        for i in range(nbits):
            self.comb += data_oe[i].eq(self._oe.storage[i])
            self.comb += data_out[i].eq(self._out.storage[i])
            self.comb += self._in.status[i].eq(data_in[i])

        if with_irq:
            self.add_irq(self._in.status)

# CRG ----------------------------------------------------------------------------------------------

class _CRG(Module):
    def __init__(self, platform, sys_clk_freq, cpu_clk_freq, with_dfs=False, dfs_pll_frequency_1=100e6, dfs_pll_frequency_2=100e6):
        self.rst = Signal()
        self.clock_domains.cd_init      = ClockDomain()
        self.clock_domains.cd_por       = ClockDomain(reset_less=True)
        self.clock_domains.cd_sys       = ClockDomain()
        self.clock_domains.cd_sys2x     = ClockDomain()
        self.clock_domains.cd_sys2x_i   = ClockDomain(reset_less=True)
        self.clock_domains.cd_cpu       = ClockDomain()
        self.clock_domains.cd_dvo       = ClockDomain(reset_less=True)
        self.clock_domains.cd_cpupll_o1 = ClockDomain()
        self.clock_domains.cd_cpupll_o2 = ClockDomain()
        self.clock_domains.cd_cpupll_1  = ClockDomain()
        self.clock_domains.cd_cpupll_2  = ClockDomain()
        self.clock_domains.cd_rng_pll   = ClockDomain()

        # # #
        self.stop  = Signal()
        self.reset = Signal()
        self.cpupll_cd_reset = Signal()
        self.cpu_cd_nest_rst_out = Signal()
        self.cpu_cd_core_rst_out = Signal()

        # Clk / Rst
        clk125 = platform.request("clk125")
        rst_n  = platform.request("rst_n")

        # Power on reset
        por_count = Signal(16, reset=2**16-1)
        por_done  = Signal()
        self.comb += por_done.eq(por_count == 0)
        self.sync.por += If(~por_done, por_count.eq(por_count - 1))
        self.cd_por.clk.attr.add("noglobal")

        # PLL 0
        sys2x_clk_ecsout = Signal()
        self.submodules.pll0 = pll0 = ECP5PLL()
        self.comb += pll0.reset.eq(~por_done | ~rst_n | self.rst)
        pll0.register_clkin(clk125, 125e6)
        pll0.create_clkout(self.cd_sys2x_i, 2*sys_clk_freq)
        pll0.create_clkout(self.cd_init, 25e6)
        self.specials += [
            Instance("OSCG",
                p_DIV = 128,                # 2.4MHz
                o_OSC = self.cd_por.clk),
            Instance("ECLKBRIDGECS",
                i_CLK0   = self.cd_sys2x_i.clk,
                i_SEL    = 0,
                o_ECSOUT = sys2x_clk_ecsout),
            Instance("ECLKSYNCB",
                i_ECLKI = sys2x_clk_ecsout,
                i_STOP  = self.stop,
                o_ECLKO = self.cd_sys2x.clk),
            Instance("CLKDIVF",
                p_DIV     = "2.0",
                i_ALIGNWD = 0,
                i_CLKI    = self.cd_sys2x.clk,
                i_RST     = self.reset,
                o_CDIVX   = self.cd_sys.clk),
        ]

        # Generate DVO clock
        self.dvo_frequency = 40e6                         # 800x600@60
        #self.dvo_frequency = 148.35e6                    # 1920x1080@60
        #self.dvo_frequency = 148.2e6                     # 1920x1200@60
        pll0.create_clkout(self.cd_dvo, self.dvo_frequency)

        # PLL 1
        # CPU PLL
        # Hold this PLL in reset until PLL0 locks and the clock adjustment logic (self.stop) has
        # finished synchronizing the edge clocks.  Failure to do so will result in the CPU coming
        # out of reset before the nest is ready to support operation, leading to an immediate crash
        # on startup that is subsequently clearable by asserting the external CPU reset signal...
        self.submodules.pll1 = pll1 = ECP5PLL()
        self.comb += pll1.reset.eq(~por_done | ~rst_n | ~pll0.locked | self.stop | self.rst)
        pll1.register_clkin(clk125, 125e6)

        if with_dfs:
            # Generate DFS base clock
            pll1.create_clkout(self.cd_cpupll_o1, dfs_pll_frequency_1)
            pll1.create_clkout(self.cd_cpupll_o2, dfs_pll_frequency_2)
            self.comb += self.cd_cpupll_1.clk.eq(self.cd_cpupll_o1.clk)
            self.comb += self.cd_cpupll_2.clk.eq(self.cd_cpupll_o2.clk)

            self.specials += [
                AsyncResetSynchronizer(self.cd_cpupll_1, ~pll0.locked | ~pll1.locked | self.reset),
                AsyncResetSynchronizer(self.cd_cpupll_2, ~pll0.locked | ~pll1.locked | self.reset),
            ]

            # Wire RNG PLL to CPU RNG core
            self.comb += self.cd_rng_pll.clk.eq(self.cd_sys.clk)
        else:
            # Generate and constrain CPU clock
            pll1.create_clkout(self.cd_cpupll_o1, cpu_clk_freq)
            self.comb += self.cd_cpu.clk.eq(self.cd_cpupll_o1.clk)
            platform.add_period_constraint(self.cd_cpu.clk, 1e9/cpu_clk_freq)

            # Generate CPU RNG core PLL clock
            pll1.create_clkout(self.cd_rng_pll, cpu_clk_freq * 1.07)

        self.specials += [
            AsyncResetSynchronizer(self.cd_sys,   ~pll0.locked | ~pll1.locked | self.cpu_cd_nest_rst_out | self.reset),
            AsyncResetSynchronizer(self.cd_sys2x, ~pll0.locked | ~pll1.locked | self.cpu_cd_nest_rst_out | self.reset),
            AsyncResetSynchronizer(self.cd_cpu,   ~pll0.locked | ~pll1.locked | self.cpu_cd_nest_rst_out | self.reset),
        ]

# BaseSoC ------------------------------------------------------------------------------------------

class BaseSoC(SoCCore):
    mem_map = {
        "hostxicsicp"     : 0xc3000000,
        "hostxicsics"     : 0xc3001000,
        "ethmac"          : 0xc3002000,
        "fpgaspiflashcfg" : 0xc3004000,
        "bmcspiflashcfg"  : 0xc3004100,
        "hostspiflashcfg" : 0xc3004200,
        "simplertc"       : 0xc3005000,
        "simpledfs"       : 0xc3005800,
        "simplepwm"       : 0xc3006800,
        "openfsimaster"   : 0xc3007000,
        "i2cmaster0"      : 0xc3008000,
        "i2cmaster1"      : 0xc3008020,
        "i2cmaster2"      : 0xc3008040,
        "i2cmaster3"      : 0xc3008060,
        "i2cmaster4"      : 0xc3008080,
        "i2cmaster5"      : 0xc30080a0,
        "i2cmaster6"      : 0xc30080c0,
        "onewire0"        : 0xc3008800,
        "externaladc"     : 0xc300a000,
        "fpgaspiflash"    : 0xc4000000,
        "bmcspiflash"     : 0xc6000000,
        "hostspiflash"    : 0xc8000000,
        "hostlpcslave"    : 0xcc000000,
    }
    mem_map.update(SoCCore.mem_map)

    interrupt_map = {
        "gpio1"         : 1,
        "ethmac"        : 2,
        "hostlpcslave"  : 3,
        "openfsimaster" : 4,
        "i2cmaster0"    : 5,
        "i2cmaster1"    : 6,
        "i2cmaster2"    : 7,
        "i2cmaster3"    : 8,
        "watchdog0"     : 9,
        "onewire0"      : 10,
    }
    interrupt_map.update(SoCCore.interrupt_map)

    def __init__(self, sys_clk_freq=int(60e6), cpu_clk_freq=int(60e6), device="LFE5UM5G", with_video=False, with_ethernet=False, with_etherbone=False, with_openfsi_master=False, with_fpgaspiflash=False, with_bmcspiflash=False, with_hostspiflash=False, with_externaladc=False, with_hostlpcslave=False, with_i2c_masters=False, with_onewire_masters=False, with_gpio=False, with_simple_pwm=False, with_simple_rtc=False, uart_count=2, with_litescope=False, with_uartbone=False, with_simpledfs=False, eth_ip="192.168.1.50", eth_phy=0, remote_ip="192.168.1.100", toolchain="trellis", **kwargs):
        platform = rcs_arctic_tern_bmc_card.Platform(toolchain=toolchain, device=device)

        # Reduce resource wastage on ECP5
        kwargs["csr_address_width"] = 15
        kwargs["csr_data_width"] = 8
        kwargs["csr_paging"] = 0x800

        # Disable timer peripheral
        # Microwatt, or any other POWER-compliant CPU, already provides an
        # architecturally-defined timer with interrupt (decrementer),
        # so the LiteX one just wastes resources in Kestrel.
        kwargs["with_timer"] = False
        kwargs["timer_uptime"] = False

        # Enable watchdog
        kwargs["with_watchdog"] = True

        # FIXME: adapt integrated rom size for Microwatt
        if kwargs.get("cpu_type", None) == "microwatt" or kwargs.get("cpu_type", None) == "libresoc":
            kwargs["integrated_rom_size"] = 0xe000 if with_ethernet else 0xc000
        else:
            # There are numerous PowerPC-isms scattered throughout the HDL and firmware.
            # Even if you get a bistream out with a non-PowerPC CPU, it probably won't
            # work, and the firmware almost certainly won't build, let alone function.
            #
            # If you are an end user or software developer, you probably forgot to pass
            # "--cpu-type=microwatt" to the build script.
            #
            # If you are a developer and are trying to port the Kestrel HDL and firmware
            # to a non-PowerPC CPU, you probably know what you're doing and can debug
            # whatever you break on your own hardware.  Remove this line and keep hacking!
            raise OSError("Kestrel HDL and firmware currently require a PowerPC-compatible CPU to function.  Did you forget '--cpu-type=microwatt'?")

        # SoCCore -----------------------------------------_----------------------------------------
        SoCCore.__init__(self, platform, irq_n_irqs=16, clk_freq=sys_clk_freq, cpu_freq=cpu_clk_freq,
            ident          = "Kestrel SoC on Raptor Arctic Tern",
            ident_version  = True,
            **kwargs)

        # CRG --------------------------------------------------------------------------------------
        self.submodules.crg = _CRG(platform, sys_clk_freq, cpu_clk_freq, with_dfs=with_simpledfs, dfs_pll_frequency_1=cpu_clk_freq*2.0, dfs_pll_frequency_2=cpu_clk_freq*1.5)

        # External CPU reset
        self.comb += self.cpu.ext_reset.eq(self.crg.cpu_cd_core_rst_out)

        # Enable early BIOS initialization routines
        self.add_constant("TARGET_BIOS_EARLY_INIT", 1)

        # DFS core ---------------------------------------------------------------------------------
        if with_simpledfs:
            from simpledfs.simpledfs import SimpleDFSSlave

            self.submodules.simple_dfs = SimpleDFSSlave(
                platform              = platform,
                cpu_pll_clock_in_1    = self.crg.cd_cpupll_1,
                cpu_pll_cd_reset_in_1 = self.crg.cd_cpupll_1.rst,
                cpu_pll_clock_in_2    = self.crg.cd_cpupll_2,
                cpu_pll_cd_reset_in_2 = self.crg.cd_cpupll_2.rst,
                cpu_logic_clock_out   = self.crg.cd_cpu,
                cpu_nest_reset_out    = self.crg.cpu_cd_nest_rst_out,
                cpu_core_reset_out    = self.crg.cpu_cd_core_rst_out,
                endianness            = self.cpu.endianness,
                cpu_clk_freq          = cpu_clk_freq,
                pll_clk_freq_1        = cpu_clk_freq * 2.0,
                pll_clk_freq_2        = cpu_clk_freq * 1.5)
            simple_dfs_size = 128
            simple_dfs_region = SoCRegion(origin=self.mem_map.get("simpledfs", None), size=simple_dfs_size, cached=False)
            self.bus.add_slave(name="simpledfs", slave=self.simple_dfs.slave_bus, region=simple_dfs_region)
        else:
             self.comb += self.cpu.ext_reset.eq(self.crg.cd_cpu.rst)

        # Video Output -----------------------------------------------------------------------------
        if with_video:
            dvo_pads = platform.request("dvo")

            self.submodules.videophy = VideoDVOPHY(dvo_pads, clock_domain="dvo")
            #self.add_video_colorbars(phy=self.videophy, timings="800x600@60Hz", clock_domain="dvo")
            #self.add_video_colorbars(phy=self.videophy, timings="1920x1080@60Hz", clock_domain="dvo")
            #self.add_video_colorbars(phy=self.videophy, timings="1920x1200@60Hz", clock_domain="dvo")
            self.add_video_terminal(phy=self.videophy, timings="800x600@60Hz", clock_domain="dvo")

        # DDR3 SDRAM --------------------------------------------------------------------------------
        if not self.integrated_main_ram_size:
            self.submodules.ddrphy = ECP5DDRPHY(
                platform.request("ddram"),
                cmd_delay=0 if sys_clk_freq > 64e6 else 100,
                sys_clk_freq=sys_clk_freq)

            # Try to place the DDR controller at a known memory location
            self.csr.locs["ddrphy"] = 7

            self.add_csr("ddrphy", use_loc_if_exists=True)
            self.comb += self.crg.stop.eq(self.ddrphy.init.stop)
            self.comb += self.crg.reset.eq(self.ddrphy.init.reset)
            self.add_sdram("sdram",
                phy                     = self.ddrphy,
                module                  = MT41J256M16(sys_clk_freq, "1:2"), # Not MT41J256M16, but the AS4C256M16D3C in use has similar specifications
                origin                  = self.mem_map["main_ram"],
                size                    = kwargs.get("max_sdram_size", 0x40000000),
                l2_cache_size           = kwargs.get("l2_size", 8192),
                l2_cache_min_data_width = kwargs.get("min_l2_data_width", 128),
                l2_cache_reverse        = True
            )

        # Ethernet / Etherbone ---------------------------------------------------------------------
        if with_ethernet or with_etherbone:
            self.submodules.ethphy = LiteEthPHYRGMII(
                clock_pads = self.platform.request("eth_clocks"),
                pads       = self.platform.request("eth"))

            # Try to place the Ethernet PHY at a known memory location
            self.csr.locs["ethphy"] = 9

            self.add_csr("ethphy", use_loc_if_exists=True)
            self.add_constant("TARGET_BIOS_ETH_INIT", 1)
            if with_ethernet:
                self.add_ethernet(phy=self.ethphy, dynamic_ip=False)
                for i in range(4):
                    self.add_constant("LOCALIP{}".format(i+1), int(eth_ip.split(".")[i]))
                for i in range(4):
                    self.add_constant("REMOTEIP{}".format(i+1), int(remote_ip.split(".")[i]))
            if with_etherbone:
                self.add_etherbone(phy=self.ethphy, ip_address=eth_ip)

        # FPGA SPI Flash (Tercel core) -------------------------------------------------------------
        if with_fpgaspiflash:
            fpgaspiflash4x_pads = platform.request("fpgaspiflash4x")
            self.fpga_spi_dq_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.fpgaspiflash   = TercelSPI(
                platform             = platform,
                pads                 = fpgaspiflash4x_pads,
                lattice_ecp5_usrmclk = True,
                lock_phy_to_bus_clk  = False,
                debug_signals        = self.fpga_spi_dq_debug,
                clk_freq             = sys_clk_freq,
                endianness           = self.cpu.endianness,
                adr_offset           = self.mem_map.get("fpgaspiflash", None))

            fpgaspiflash_size         = 16*mB
            fpgaspiflash_region       = SoCRegion(origin=self.mem_map.get("fpgaspiflash", None), size=fpgaspiflash_size, cached=False)
            self.bus.add_slave(name="fpgaspiflash", slave=self.fpgaspiflash.bus, region=fpgaspiflash_region)
            fpgaspiflashcfg_size      = 128
            fpgaspiflashcfg_region    = SoCRegion(origin=self.mem_map.get("fpgaspiflashcfg", None), size=fpgaspiflashcfg_size, cached=False)
            self.bus.add_slave(name="fpgaspiflashcfg", slave=self.fpgaspiflash.cfg_bus, region=fpgaspiflashcfg_region)

        # BMC SPI Flash (Tercel core) -------------------------------------------------------------
        if with_bmcspiflash:
            bmcspiflash4x_pads = platform.request("bmcspiflash4x")
            self.bmc_spi_dq_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.bmcspiflash   = TercelSPI(
                platform             = platform,
                pads                 = bmcspiflash4x_pads,
                lattice_ecp5_usrmclk = False,
                lock_phy_to_bus_clk  = False,
                debug_signals        = self.bmc_spi_dq_debug,
                clk_freq             = sys_clk_freq,
                endianness           = self.cpu.endianness,
                adr_offset           = self.mem_map.get("bmcspiflash", None))

            bmcspiflash_size         = 16*mB
            bmcspiflash_region       = SoCRegion(origin=self.mem_map.get("bmcspiflash", None), size=bmcspiflash_size, cached=False)
            self.bus.add_slave(name="bmcspiflash", slave=self.bmcspiflash.bus, region=bmcspiflash_region)
            bmcspiflashcfg_size      = 128
            bmcspiflashcfg_region    = SoCRegion(origin=self.mem_map.get("bmcspiflashcfg", None), size=bmcspiflashcfg_size, cached=False)
            self.bus.add_slave(name="bmcspiflashcfg", slave=self.bmcspiflash.cfg_bus, region=bmcspiflashcfg_region)

        # Host SPI Flash (Tercel core) -------------------------------------------------------------
        if with_hostspiflash:
            hostspiflash4x_pads = platform.request("hostspiflash4x")
            self.host_spi_dq_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.hostspiflash   = TercelSPI(
                platform             = platform,
                pads                 = hostspiflash4x_pads,
                lattice_ecp5_usrmclk = False,
                lock_phy_to_bus_clk  = False,
                debug_signals        = self.host_spi_dq_debug,
                clk_freq             = sys_clk_freq,
                endianness           = self.cpu.endianness,
                adr_offset           = self.mem_map.get("hostspiflash", None))

            hostspiflash_size        = 64*mB
            hostspiflash_region      = SoCRegion(origin=self.mem_map.get("hostspiflash", None), size=hostspiflash_size, cached=False)
            self.bus.add_slave(name="hostspiflash", slave=self.hostspiflash.bus, region=hostspiflash_region)
            hostspiflashcfg_size     = 128
            hostspiflashcfg_region   = SoCRegion(origin=self.mem_map.get("hostspiflashcfg", None), size=hostspiflashcfg_size, cached=False)
            self.bus.add_slave(name="hostspiflashcfg", slave=self.hostspiflash.cfg_bus, region=hostspiflashcfg_region)

        # External ADC (Tercel core) ---------------------------------------------------------------
        if with_externaladc:
            external_adc_pads = platform.request("j300_spi")
            self.external_adc_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.externaladc   = ADS7950Driver(
                platform             = platform,
                pads                 = external_adc_pads,
                lattice_ecp5_usrmclk = False,
                debug_signals        = self.external_adc_debug,
                clk_freq             = sys_clk_freq,
                endianness           = self.cpu.endianness,
                adr_offset           = self.mem_map.get("externaladc", None))

            # Try to place the external ADC at a known memory location
            self.csr.locs["externaladc"] = 26

            self.add_csr("externaladc", use_loc_if_exists=True)
            externaladc_size     = 128
            externaladc_region   = SoCRegion(origin=self.mem_map.get("externaladc", None), size=externaladc_size, cached=False)
            self.bus.add_slave(name="externaladc", slave=self.externaladc.bus, region=externaladc_region)

        # Host LPC Slave (Aquila core) -------------------------------------------------------------
        if with_hostlpcslave:
            hostlpcslave_pads = platform.request("hostlpcslave")
            self.host_lpc_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.host_lpc_clock_mirror = Signal()
            self.submodules.hostlpcslave   = AquilaLPCSlave(
                platform       = platform,
                pads           = hostlpcslave_pads,
                clk_freq       = sys_clk_freq,
                debug_signals  = self.host_lpc_debug,
                lpc_clk_mirror = self.host_lpc_clock_mirror,
                endianness     = self.cpu.endianness,
                adr_offset     = self.mem_map.get("hostlpcslave", None))

            # Try to place the LPC slave at a known memory location
            self.csr.locs["hostlpcslave"] = 14

            self.add_csr("hostlpcslave", use_loc_if_exists=True)
            hostlpcslave_size   = 16*mB
            hostlpcslave_region = SoCRegion(origin=self.mem_map.get("hostlpcslave", None), size=hostlpcslave_size, cached=False)
            self.bus.add_slave(name="hostlpcslave", slave=self.hostlpcslave.slave_bus, region=hostlpcslave_region)
            self.bus.add_master(name="hostlpcslave", master=self.hostlpcslave.master_bus)

        # OpenFSI Master ---------------------------------------------------------------------------
        if with_openfsi_master:
            openfsi_master_pads = platform.request("openfsi_master")
            self.submodules.openfsi_master = OpenFSIMaster(
                platform        = platform,
                pads            = openfsi_master_pads,
                clk_freq        = sys_clk_freq,
                endianness      = self.cpu.endianness)

            # Try to place the OpenFSI master at a known memory location
            self.csr.locs["openfsimaster"] = 15

            self.add_csr("openfsimaster", use_loc_if_exists=True)
            openfsi_master_size = 128
            openfsi_master_region = SoCRegion(origin=self.mem_map.get("openfsimaster", None), size=openfsi_master_size, cached=False)
            self.bus.add_slave(name="openfsimaster", slave=self.openfsi_master.slave_bus, region=openfsi_master_region)

        # I2C Masters ------------------------------------------------------------------------------
        if with_i2c_masters:
            for i2c_master_id, i2c_master_pin_group in [(0,0),(1,1),(2,2),(4,6)]:
                i2c_master_pads = platform.request("i2c_master", i2c_master_pin_group, True)
                if not i2c_master_pads:
                    continue

                master_name = "i2cmaster%d" % i2c_master_id

                i2cmaster = OpenCoresI2CMaster(
                    platform        = platform,
                    pads            = i2c_master_pads,
                    clk_freq        = sys_clk_freq)
                setattr(self.submodules, master_name, i2cmaster)

                # Try to place the I2C controllers at a known memory location
                self.csr.locs[master_name] = 16 + i2c_master_id

                self.add_csr(master_name, use_loc_if_exists=True)
                i2cmaster_size = 32
                i2cmaster_region = SoCRegion(origin=self.mem_map.get(master_name, None), size=i2cmaster_size, cached=False)
                self.bus.add_slave(name=master_name, slave=i2cmaster.bus, region=i2cmaster_region)

        # OneWire Master ---------------------------------------------------------------------------
        if with_onewire_masters:
            onewire_master_pads = platform.request("j300_onewire", 0, True)

            master_name = "onewire0"

            onewire_master = OpenCores1WireMaster(
                platform         = platform,
                pads             = onewire_master_pads,
                clk_freq         = sys_clk_freq,
                with_power_drive = False)
            setattr(self.submodules, master_name, onewire_master)

            # Try to place the OneWire master at a known memory location
            self.csr.locs["onewire0"] = 25

            self.add_csr(master_name, use_loc_if_exists=True)
            onewire_master_size = 32
            onewire_master_region = SoCRegion(origin=self.mem_map.get(master_name, None), size=onewire_master_size, cached=False)
            self.bus.add_slave(name=master_name, slave=onewire_master.slave_bus, region=onewire_master_region)

        # SimplePWM --------------------------------------------------------------------------------
        if with_simple_pwm:
            pwm_tach_pads = platform.request("pwm_tach_pads")
            self.submodules.simple_pwm = SimplePWMSlave(
                platform        = platform,
                pads            = pwm_tach_pads,
                endianness      = self.cpu.endianness,
                pwm_clk_src     = 'sys',
                pwm_clk_freq    = sys_clk_freq)

            # Try to place the PWM controller at a known memory location
            self.csr.locs["simplepwm"] = 23

            self.add_csr("simplepwm", use_loc_if_exists=True)
            simple_pwm_size = 128
            simple_pwm_region = SoCRegion(origin=self.mem_map.get("simplepwm", None), size=simple_pwm_size, cached=False)
            self.bus.add_slave(name="simplepwm", slave=self.simple_pwm.slave_bus, region=simple_pwm_region)

        # SimpleRTC --------------------------------------------------------------------------------
        if with_simple_rtc:
            self.submodules.simple_rtc = SimpleRTCSlave(
                platform        = platform,
                endianness      = self.cpu.endianness,
                rtc_clk_src     = 'sys',
                rtc_clk_freq    = sys_clk_freq)

            # Try to place the RTC at a known memory location
            self.csr.locs["simplertc"] = 13

            self.add_csr("simplertc", use_loc_if_exists=True)
            simple_rtc_size = 128
            simple_rtc_region = SoCRegion(origin=self.mem_map.get("simplertc", None), size=simple_rtc_size, cached=False)
            self.bus.add_slave(name="simplertc", slave=self.simple_rtc.slave_bus, region=simple_rtc_region)

        # GPIO Driver (Bank 1, I2C bus 5) ----------------------------------------------------------
        if with_gpio:
            i2c_master_pads = platform.request("i2c_master", 4, True)

            self.gpio5_in = Signal(21)
            self.gpio5_direction = Signal(21)
            self.gpio5_direction_to_driver = Signal(21)
            self.gpio5_out = Signal(21)
            self.gpio5_out_to_driver = Signal(21)
            self.gpio5_digit_sync = Signal()

            # Dot points are connected below
            # Connect all other signals to the GPIO 5 driver bank
            self.comb += self.gpio5_direction_to_driver[4:18].eq(self.gpio5_direction[4:18])
            self.comb += self.gpio5_out_to_driver[4:18].eq(self.gpio5_out[4:18])

            gpiocontroller = ArcticTernGPIOI2CExists(
                platform        = platform,
                pads            = i2c_master_pads,
                gpio_in         = self.gpio5_in[0:18],
                gpio_direction  = self.gpio5_direction_to_driver[0:18],
                gpio_out        = self.gpio5_out_to_driver[0:18],
                digit_sync      = self.gpio5_digit_sync,
                clk_freq        = sys_clk_freq)
            setattr(self.submodules, "gpiocontroller5", gpiocontroller)

            # Bit 18 switches SDR display to raw (ASCII) mode
            # Bits 19-20 switch the SDR display to CPU register debug mode
            self.comb += self.gpio5_in[18:21].eq(self.gpio5_out[18:21])

            # Try to place the GPIO controller at a known memory location
            self.csr.locs["gpio1"] = 11

            self.submodules.gpio1 = InternalGPIOTristate(
                data_in      = self.gpio5_in,
                data_oe      = self.gpio5_direction,
                data_out     = self.gpio5_out,
                nbits        = 21,
                with_irq     = True)
            self.add_csr("gpio1", use_loc_if_exists=True)

        # GPIO Driver (Bank 2, I2C bus 4) ----------------------------------------------------------
        if with_gpio:
            i2c_master_pads = platform.request("i2c_master", 3, True)

            self.gpio4_in = Signal(18)
            self.gpio4_direction = Signal(18)
            self.gpio4_out = Signal(18)
            self.sdr_display_word = Signal(32)
            self.gpio2_out = Signal(32)
            self.sdr_digit_sync = Signal()

            gpiocontroller = ArcticTernGPIOI2CExists(
                platform        = platform,
                pads            = i2c_master_pads,
                gpio_in         = self.gpio4_in,
                gpio_direction  = self.gpio4_direction,
                gpio_out        = self.gpio4_out,
                digit_sync      = self.sdr_digit_sync,
                clk_freq        = sys_clk_freq)
            setattr(self.submodules, "gpiocontroller4", gpiocontroller)

            sdrhexcontroller = ArcticTernSDRHexDriver(
                platform        = platform,
                display_word    = self.sdr_display_word,
                raw_mode        = self.gpio5_out[18:19],
                anodes          = self.gpio4_out[0:14],
                cathodes        = self.gpio4_out[14:18],
                digit_sync      = self.sdr_digit_sync)
            setattr(self.submodules, "sdrhexcontroller", sdrhexcontroller)

            # Set all GPIO lines to output mode
            self.comb += self.gpio4_direction.eq(0x3ffff)

            # Connect GPIO2 to SDR display driver
            self.submodules.gpio2 = InternalGPIOOut(
                data_out     = self.gpio2_out,
                nbits        = 32)

            # Try to place the GPIO controller at a known memory location
            self.csr.locs["gpio2"] = 12

            self.add_csr("gpio2", use_loc_if_exists=True)

            # Bits 19 and 20 switch SDR display to PC or LR debug mode
            #
            # When in this mode, the Arctic Tern alphanumeric display shows up to
            # 20 bits of address information.  This is very useful for debugging
            # during OS bringup, if the PC is running see if the LR is stuck,
            # the LR often shows the function that was called right before the
            # infinite loop...
            #
            # Display is left to right, MSB left.  Dot points are the upper 4 bits:
            # MSB [dot points][hex value] LSB
            self.comb += If(self.gpio5_in[19:21] == 1,
                self.sdr_display_word.eq(self.cpu.current_pc[0:16]),
                self.gpio5_out_to_driver[0:1].eq(self.cpu.current_pc[19:20]),
                self.gpio5_out_to_driver[1:2].eq(self.cpu.current_pc[18:19]),
                self.gpio5_out_to_driver[2:3].eq(self.cpu.current_pc[17:18]),
                self.gpio5_out_to_driver[3:4].eq(self.cpu.current_pc[16:17]),
                self.gpio5_direction_to_driver[0:4].eq(0xf)
            ).Elif(self.gpio5_in[19:21] == 2,
                self.sdr_display_word.eq(self.cpu.current_lr[0:16]),
                self.gpio5_out_to_driver[0:1].eq(self.cpu.current_lr[19:20]),
                self.gpio5_out_to_driver[1:2].eq(self.cpu.current_lr[18:19]),
                self.gpio5_out_to_driver[2:3].eq(self.cpu.current_lr[17:18]),
                self.gpio5_out_to_driver[3:4].eq(self.cpu.current_lr[16:17]),
                self.gpio5_direction_to_driver[0:4].eq(0xf)
            ).Else(
                self.sdr_display_word.eq(self.gpio2_out),
                self.gpio5_out_to_driver[0:4].eq(self.gpio5_out[0:4]),
                self.gpio5_direction_to_driver[0:4].eq(self.gpio5_direction[0:4])
            )

        # Secondary UARTs ---------------------------------------------------------------------------
        if uart_count > 1:
            from litex.soc.cores import uart

            for uart_index in range(1, uart_count):
                uart_phy_name = "uartphy%d" % uart_index
                uart_name = "uart%d" % uart_index
                serial_pads = platform.request("serial", uart_index, True)

                uart_phy = uart.RS232PHY(
                    pads=serial_pads,
                    clk_freq=self.sys_clk_freq,
                    baudrate=115200,
                    with_dynamic_baudrate=True)
                uart_controller = uart.UART(
                    phy=uart_phy,
                    tx_fifo_depth=16,
                    rx_fifo_depth=16,
                )
                setattr(self.submodules, uart_phy_name, uart_phy)
                setattr(self.submodules, uart_name, uart_controller)

                # Try to place the UARTs at a known memory location
                self.csr.locs[uart_name] = 24 + (uart_index * 2)
                self.csr.locs[uart_phy_name] = 25 + (uart_index * 2)

                # Try to place the UARTs at a known interrupt location
                self.irq.locs[uart_name] = 10 + uart_index

                self.add_csr(uart_name, use_loc_if_exists=True)
                self.add_csr(uart_phy_name, use_loc_if_exists=True)
                self.irq.add(uart_name, use_loc_if_exists=True)

        # CPU Analyzer -----------------------------------------------------------------------------
        if with_litescope:
            analyzer_signals = [
                # System state
                self.crg.cd_sys.clk,
                self.crg.cd_cpu.clk,
                self.cpu.ext_reset,
                self.crg.cd_sys.rst,
                self.crg.cd_cpu.rst,
                self.crg.pll0.locked,
                self.crg.pll1.locked,

                # CPU state
                self.cpu.current_pc,
                self.cpu.current_lr,
                self.cpu.core_ext_irq,
                self.cpu.core_ext_irq_sync,

                # Instruction bus (CPU side)
                self.cpu.wb_insn.stb,
                self.cpu.wb_insn.cyc,
                self.cpu.wb_insn.adr,
                self.cpu.wb_insn.we,
                self.cpu.wb_insn.ack,
                self.cpu.wb_insn.stall,
                self.cpu.wb_insn.sel,
                self.cpu.wb_insn.dat_w,
                self.cpu.wb_insn.dat_r,

                # Data bus (CPU side)
                self.cpu.wb_data.stb,
                self.cpu.wb_data.cyc,
                self.cpu.wb_data.adr,
                self.cpu.wb_data.we,
                self.cpu.wb_data.ack,
                self.cpu.wb_data.stall,
                self.cpu.wb_data.sel,
                self.cpu.wb_data.dat_w,
                self.cpu.wb_data.dat_r,

                # Instruction bus (Wishbone side)
                self.cpu.wb_ext_insn.stb,
                self.cpu.wb_ext_insn.cyc,
                self.cpu.wb_ext_insn.adr,
                self.cpu.wb_ext_insn.we,
                self.cpu.wb_ext_insn.ack,
                self.cpu.wb_ext_insn.stall,
                self.cpu.wb_ext_insn.sel,
                self.cpu.wb_ext_insn.dat_w,
                self.cpu.wb_ext_insn.dat_r,

                # Data bus (Wishbone side)
                self.cpu.wb_ext_data.stb,
                self.cpu.wb_ext_data.cyc,
                self.cpu.wb_ext_data.adr,
                self.cpu.wb_ext_data.we,
                self.cpu.wb_ext_data.ack,
                self.cpu.wb_ext_data.stall,
                self.cpu.wb_ext_data.sel,
                self.cpu.wb_ext_data.dat_w,
                self.cpu.wb_ext_data.dat_r,
            ]

            from litescope import LiteScopeAnalyzer
            self.submodules.analyzer = LiteScopeAnalyzer(analyzer_signals,
                depth          = 512,
                clock_domain   = "sys",
                samplerate     = self.sys_clk_freq,
                csr_csv        = "analyzer.csv",
                csr_data_width = self.csr.data_width
            )
            self.add_csr("analyzer")

        # UART to Wishbone master bridge -----------------------------------------------------------
        if with_uartbone:
            from litex.soc.cores import uart
            self.submodules.uartbone_phy = uart.UARTPHY(platform.request("serial", 1), self.sys_clk_freq, 115200)
            self.csr.add("uartbone_phy")
            self.submodules.uartbone = uart.UARTBone(phy=self.uartbone_phy, clk_freq=self.sys_clk_freq, cd="sys")
            self.bus.add_master(name="uartbone", master=self.uartbone.wishbone)

    def set_gateware_dir(self, gateware_dir):
        self.gateware_dir = gateware_dir

    def initialize_rom(self, data):
        # Save actual expected contents for future use as gateware/rom.init
        content = ""
        formatter = "{:0" + str(int(self.rom.mem.width / 4)) + "X}\n"
        for d in data:
            content += formatter.format(d).zfill(int(self.rom.mem.width / 4))
        romfile = os.open(os.path.join(self.gateware_dir, "rom_data.init"), os.O_WRONLY | os.O_CREAT)
        os.write(romfile, content.encode())
        os.close(romfile)

        # Generate initial data to allow ecpbram to later stuff the bitstream
        (_, path) = tempfile.mkstemp()
        subprocess.check_call(["ecpbram", "-g", path, "-w", str(self.rom.mem.width), "-d", str(int(self.integrated_rom_size / 4)), "-s" "0"])

        # Convert data to binary
        random_file = open(path, 'r')
        data = []
        random_lines = random_file.readlines()
        for line in random_lines:
            data.append(int(line, 16))

        os.remove(path)

        self.rom.mem.init = data
        self.rom.mem.name_override = "rom"

# Build --------------------------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(description="LiteX SoC on Arctic Tern (BMC card carrier)")
    parser.add_argument("--build",           action="store_true",               help="Build bitstream")
    parser.add_argument("--load",            action="store_true",               help="Load bitstream")
    parser.add_argument("--toolchain",       default="trellis",                 help="FPGA toolchain: trellis (default) or diamond")
    parser.add_argument("--sys-clk-freq",    default=60e6,                      help="System clock frequency (default: 60MHz)")
    parser.add_argument("--cpu-clk-freq",    default=60e6,                      help="CPU clock frequency (default: 60MHz)")
    parser.add_argument("--device",          default="LFE5UM5G",                help="FPGA device (LFE5UM5G (default) or LFE5UM)")
    parser.add_argument("--with-video",      action="store_true",               help="Enable video support")
    ethopts = parser.add_mutually_exclusive_group()
    ethopts.add_argument("--with-ethernet",  action="store_true",               help="Enable Ethernet support")
    ethopts.add_argument("--with-etherbone", action="store_true",               help="Enable Etherbone support")
    parser.add_argument("--eth-ip",          default="192.168.1.50", type=str,  help="Ethernet/Etherbone IP address")
    parser.add_argument("--eth-phy",         default=0, type=int,               help="Ethernet PHY: 0 (default) or 1")
    parser.add_argument("--remote-ip",       default="192.168.1.100", type=str, help="Remote firmware server IP address")
    parser.add_argument("--production",      action="store_true",               help="Enable production mode BIOS")
    builder_args(parser)
    soc_sdram_args(parser)
    trellis_args(parser)
    args = parser.parse_args()

    soc = BaseSoC(
        sys_clk_freq   = int(float(args.sys_clk_freq)),
        cpu_clk_freq   = int(float(args.cpu_clk_freq)),
        device         = args.device,
        with_video     = args.with_video,
        with_ethernet  = args.with_ethernet,
        with_etherbone = args.with_etherbone,
        with_fpgaspiflash = True,
        with_bmcspiflash = True,
        with_hostspiflash = True,
        with_externaladc = False,
        with_hostlpcslave = True,
        with_openfsi_master = True,
        with_i2c_masters = True,
        with_onewire_masters = False,
        with_gpio = True,
        with_simple_pwm = True,
        with_simple_rtc = True,
        uart_count = 2,
        with_litescope = False,
        with_uartbone = False,
        with_simpledfs = False,
        eth_ip         = args.eth_ip,
        eth_phy        = args.eth_phy,
        remote_ip      = args.remote_ip,
        toolchain      = args.toolchain,
        **soc_sdram_argdict(args)
    )
    builder = Builder(soc, **builder_argdict(args))

    # Production mode
    # When enabled, boot time is prioritized
    # and internal Flash is used as the boot source
    # Various self-tests are disabled as well
    production_mode = args.production

    # Target-specific early initialization routines
    os.makedirs(os.path.join(builder.software_dir, "include/generated"),
                 exist_ok=True)
    write_to_file(
         os.path.join(builder.software_dir, "include/generated", "target_bios.h"),
             "void target_bios_early_init() {\n"
             "#ifdef SIMPLEDFS_BASE\n"
             "    // Set CPU core to full speed\n"
             "    *((uint32_t*)(SIMPLEDFS_BASE+0x1c)) = 0x0;\n"
             "#endif\n"
             "}\n")

    if args.with_ethernet:
        # The KSZ9021RL PHY requires additional setup to
        # reset the PHY, enable the RX clock delay, and
        # set the correct RGMII RXD lane skew...
        #
        # Without the lane skew set correctly, insufficient
        # margin exists to allow for a reliable RGMII link
        # under PVT variations.
        os.makedirs(os.path.join(builder.software_dir, "include/generated"),
                     exist_ok=True)
        write_to_file(
             os.path.join(builder.software_dir, "include/generated", "target_eth.h"),
                 "// Arctic Tern needs this sequence to reset the PHY and enable RX data to clock delay.\n"
                 "void target_eth_init() {\n"
                 "    // Configure PHY reset GPIO line\n"
                 "    gpio1_oe_write(gpio1_oe_read() | (0x1 << 15));\n"
                 "    // Reset KSZ9021RL PHY\n"
                 "    gpio1_out_write(gpio1_out_read() & ~(0x1 << 15));\n"
                 "    // Keep PHY reset asserted for 200ms\n"
                 "    busy_wait(200);\n"
                 "    gpio1_out_write(gpio1_out_read() | (0x1 << 15));\n"
                 "    // Wait for PHY reset cycle to complete\n"
                 "    busy_wait(200);\n"
                 "    // Enable write access to manufacturer specific register 0x104\n"
                 "    mdio_write(0, 0x0b, 0x8104);\n"
                 "    // Enable RX delay\n"
                 "    mdio_write(0, 0x0c, 0xf077);\n"
                 "    // Disable write access to manufacturer specific register 0x104\n"
                 "    mdio_write(0, 0x0b, 0x0104);\n"
                 "    // Enable write access to manufacturer specific register 0x105\n"
                 "    mdio_write(0, 0x0b, 0x8105);\n"
                 "    // Set RGMII RXD skew control to 0x7 on all four lanes\n"
                 "    mdio_write(0, 0x0c, 0x7777);\n"
                 "    // Disable write access to manufacturer specific register 0x105\n"
                 "    mdio_write(0, 0x0b, 0x0105);\n"
                 "}\n")

    # Flash boot setup
    flash_boot_adr_pri = soc.mem_map["fpgaspiflash"] + 0x400000
    flash_boot_adr_sec = soc.mem_map["fpgaspiflash"] + 0x800000
    if production_mode:
        soc.add_constant("FLASH_BOOT_ADDRESS_PRIMARY",   flash_boot_adr_pri)
        soc.add_constant("FLASH_BOOT_ADDRESS_SECONDARY", flash_boot_adr_sec)


    soc.set_gateware_dir(builder.gateware_dir)

    builder_kargs = trellis_argdict(args) if args.toolchain == "trellis" else {}
    builder.build(**builder_kargs, run=args.build)

    # Stuff the original rom into the fpga
    subprocess.check_call(["ecpbram",
                        "-i", os.path.join(builder.gateware_dir, soc.platform.name + ".config"),
                        "-o", os.path.join(builder.gateware_dir, soc.platform.name + "_stuffed.config"),
                        "-f", os.path.join(builder.gateware_dir, "rom.init"),
                        "-t", os.path.join(builder.gateware_dir, "rom_data.init")])

    # Update the svf / bit files
    subprocess.check_call(["ecppack",
                        os.path.join(builder.gateware_dir, soc.platform.name + "_stuffed.config"),
                        "--svf", os.path.join(builder.gateware_dir, soc.platform.name + ".svf"),
                        "--bit", os.path.join(builder.gateware_dir, soc.platform.name + ".bit"),
                        "--spimode", "fast-read",
                        "--freq", "38.8",
                        "--compress",
                        "--bootaddr", "0"])

    if args.load:
        prog = soc.platform.create_programmer()
        prog.load_bitstream(os.path.join(builder.gateware_dir, soc.build_name + ".svf"))

if __name__ == "__main__":
    main()
