#!/usr/bin/env python3

#
# This file is part of Kestrel
#
# Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# Copyright (c) 2020-2021 Raptor Engineering, LLC
# SPDX-License-Identifier: BSD-2-Clause

import os
import argparse
import subprocess
import tempfile

from migen import *
from migen.genlib.resetsync import AsyncResetSynchronizer

from litex_boards.platforms import versa_ecp5

from litex.build.lattice.trellis import trellis_args, trellis_argdict

from litex.soc.cores.clock import *
from litex.soc.integration.soc import SoCRegion
from litex.soc.integration.soc_core import *
from litex.soc.integration.soc_sdram import *
from litex.soc.integration.builder import *
from litex.soc.cores.led import LedChaser

from litedram.modules import MT41K64M16
from litedram.phy import ECP5DDRPHY

from liteeth.phy.ecp5rgmii import LiteEthPHYRGMII

# Kestrel-specific peripherals
from tercelspi.tercelspi import TercelSPI
from aquilalpc.aquilalpc import AquilaLPCSlave
from swiftfsi.swiftfsi import OpenFSIMaster
from simplertc.simplertc import SimpleRTCSlave
from simplepwm.simplepwm import SimplePWMSlave
from opencoresi2c.opencoresi2c import OpenCoresI2CMaster

# Useful constants
kB = 1024
mB = 1024*kB

# Production mode
# When enabled, boot time is prioritized
# and internal Flash is used as the boot source
# Various self-tests are disabled as well
production_mode = False

# CRG ----------------------------------------------------------------------------------------------

class _CRG(Module):
    def __init__(self, platform, sys_clk_freq):
        self.rst = Signal()
        self.clock_domains.cd_init    = ClockDomain()
        self.clock_domains.cd_por     = ClockDomain(reset_less=True)
        self.clock_domains.cd_sys     = ClockDomain()
        self.clock_domains.cd_sys2x   = ClockDomain()
        self.clock_domains.cd_sys2x_i = ClockDomain(reset_less=True)

        # # #

        self.stop  = Signal()
        self.reset = Signal()

        # Clk / Rst
        clk100 = platform.request("clk100")
        rst_n  = platform.request("rst_n")

        # Power on reset
        por_count = Signal(16, reset=2**16-1)
        por_done  = Signal()
        self.comb += self.cd_por.clk.eq(clk100)
        self.comb += por_done.eq(por_count == 0)
        self.sync.por += If(~por_done, por_count.eq(por_count - 1))

        # PLL
        self.submodules.pll = pll = ECP5PLL()
        self.comb += pll.reset.eq(~por_done | ~rst_n | self.rst)
        pll.register_clkin(clk100, 100e6)
        pll.create_clkout(self.cd_sys2x_i, 2*sys_clk_freq)
        pll.create_clkout(self.cd_init, 25e6)
        self.specials += [
            Instance("ECLKSYNCB",
                i_ECLKI = self.cd_sys2x_i.clk,
                i_STOP  = self.stop,
                o_ECLKO = self.cd_sys2x.clk),
            Instance("CLKDIVF",
                p_DIV     = "2.0",
                i_ALIGNWD = 0,
                i_CLKI    = self.cd_sys2x.clk,
                i_RST     = self.reset,
                o_CDIVX   = self.cd_sys.clk),
            AsyncResetSynchronizer(self.cd_sys,   ~pll.locked | self.reset),
            AsyncResetSynchronizer(self.cd_sys2x, ~pll.locked | self.reset),
        ]

# BaseSoC ------------------------------------------------------------------------------------------

class BaseSoC(SoCCore):
    mem_map = {
        "hostxicsicp"     : 0xc3000000,
        "hostxicsics"     : 0xc3001000,
        "ethmac"          : 0xc3002000,
        "bmcspiflashcfg"  : 0xc3004000,
        "hostspiflashcfg" : 0xc3005000,
        "simplertc"       : 0xc3006000,
        "simplepwm"       : 0xc3006800,
        "openfsimaster"   : 0xc3007000,
        "i2cmaster1"      : 0xc3008000,
        "i2cmaster2"      : 0xc3008020,
        "i2cmaster3"      : 0xc3008040,
        "i2cmaster4"      : 0xc3008060,
        "bmcspiflash"     : 0xc4000000,
        "hostspiflash"    : 0xc8000000,
        "hostlpcslave"    : 0xcc000000,
    }
    mem_map.update(SoCCore.mem_map)

    interrupt_map = {
        "gpio2"         : 1,
        "ethmac"        : 2,
        "hostlpcslave"  : 3,
        "openfsimaster" : 4,
        "i2cmaster1"    : 5,
        "i2cmaster2"    : 6,
        "i2cmaster3"    : 7,
        "i2cmaster4"    : 8,
    }
    interrupt_map.update(SoCCore.interrupt_map)

    def __init__(self, sys_clk_freq=int(50e6), device="LFE5UM5G", with_ethernet=False, with_etherbone=False, with_openfsi_master=True, with_bmcspiflash=True, with_hostspiflash=True, with_hostlpcslave=True, with_i2c_masters=True, with_simple_pwm=True, with_simple_rtc=True, eth_ip="192.168.1.50", eth_phy=0, toolchain="trellis", **kwargs):
        platform = versa_ecp5.Platform(toolchain=toolchain, device=device)

        # Reduce resource wastage on ECP5
        kwargs["csr_address_width"] = 14
        kwargs["csr_data_width"] = 8
        kwargs["csr_paging"] = 0x800

        # Disable timer peripheral
        # Microwatt, or any other POWER-compliant CPU, already provides an
        # architecturally-defined timer with interrupt (decrementer),
        # so the LiteX one just wastes resources in Kestrel.
        kwargs["with_timer"] = False
        kwargs["timer_uptime"] = False

        # FIXME: adapt integrated rom size for Microwatt
        if kwargs.get("cpu_type", None) == "microwatt":
            kwargs["integrated_rom_size"] = 0xd000 if with_ethernet else 0xb000
        else:
            # There are numerous PowerPC-isms scattered throughout the HDL and firmware.
            # Even if you get a bistream out with a non-PowerPC CPU, it probably won't
            # work, and the firmware almost certainly won't build, let alone function.
            #
            # If you are an end user or software developer, you probably forgot to pass
            # "--cpu-type=microwatt" to the build script.
            #
            # If you are a developer and are trying to port the Kestrel HDL and firmware
            # to a non-PowerPC CPU, you probably know what you're doing and can debug
            # whatever you break on your own hardware.  Remove this line and keep hacking!
            raise OSError("Kestrel HDL and firmware currently require a PowerPC-compatible CPU to function.  Did you forget '--cpu-type=microwatt'?")

        # SoCCore -----------------------------------------_----------------------------------------
        SoCCore.__init__(self, platform, irq_n_irqs=16, clk_freq=sys_clk_freq,
            ident          = "Kestrel SoC on Versa ECP5",
            ident_version  = True,
            **kwargs)

        # CRG --------------------------------------------------------------------------------------
        self.submodules.crg = _CRG(platform, sys_clk_freq)

        # DDR3 SDRAM -------------------------------------------------------------------------------
        if not self.integrated_main_ram_size:
            self.submodules.ddrphy = ECP5DDRPHY(
                platform.request("ddram"),
                sys_clk_freq=sys_clk_freq)
            self.add_csr("ddrphy")
            self.comb += self.crg.stop.eq(self.ddrphy.init.stop)
            self.comb += self.crg.reset.eq(self.ddrphy.init.reset)
            self.add_sdram("sdram",
                phy                     = self.ddrphy,
                module                  = MT41K64M16(sys_clk_freq, "1:2"),
                origin                  = self.mem_map["main_ram"],
                size                    = kwargs.get("max_sdram_size", 0x40000000),
                l2_cache_size           = kwargs.get("l2_size", 8192),
                l2_cache_min_data_width = kwargs.get("min_l2_data_width", 128),
                l2_cache_reverse        = True
            )

        # Ethernet / Etherbone ---------------------------------------------------------------------
        if with_ethernet or with_etherbone:
            self.submodules.ethphy = LiteEthPHYRGMII(
                clock_pads = self.platform.request("eth_clocks", eth_phy),
                pads       = self.platform.request("eth", eth_phy),
                tx_delay   = 0e-9,
                rx_delay   = 0e-9)
            self.add_csr("ethphy")
            if with_ethernet:
                self.add_ethernet(phy=self.ethphy, dynamic_ip=False)
            if with_etherbone:
                self.add_etherbone(phy=self.ethphy, ip_address=eth_ip)

        # Debug pad locator
        try:
            debug2_pads = platform.request("debug_port_2")
        except:
            debug2_pads = None
        try:
            lpc_debug_mirror_clock_pad = platform.request("lpc_debug_mirror_clock")
        except:
            lpc_debug_mirror_clock_pad = Signal()

        # BMC SPI Flash (Tercel core) -------------------------------------------------------------
        if with_bmcspiflash:
            bmcspiflash4x_pads = platform.request("bmcspiflash4x")
            self.bmc_spi_dq_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.bmcspiflash   = TercelSPI(
                platform             = platform,
                pads                 = bmcspiflash4x_pads,
                lattice_ecp5_usrmclk = True,
                debug_signals        = self.bmc_spi_dq_debug,
                clk_freq             = sys_clk_freq,
                endianness           = self.cpu.endianness,
                adr_offset           = self.mem_map.get("bmcspiflash", None))
            self.add_csr("bmcspiflash")
            bmcspiflash_size         = 16*mB
            bmcspiflash_region       = SoCRegion(origin=self.mem_map.get("bmcspiflash", None), size=bmcspiflash_size, cached=False)
            self.bus.add_slave(name="bmcspiflash", slave=self.bmcspiflash.bus, region=bmcspiflash_region)
            bmcspiflashcfg_size      = 128
            bmcspiflashcfg_region    = SoCRegion(origin=self.mem_map.get("bmcspiflashcfg", None), size=bmcspiflashcfg_size, cached=False)
            self.bus.add_slave(name="bmcspiflashcfg", slave=self.bmcspiflash.cfg_bus, region=bmcspiflashcfg_region)

        # Host SPI Flash (Tercel core) -------------------------------------------------------------
        if with_hostspiflash:
            hostspiflash4x_pads = platform.request("hostspiflash4x")
            self.host_spi_dq_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.submodules.hostspiflash   = TercelSPI(
                platform             = platform,
                pads                 = hostspiflash4x_pads,
                lattice_ecp5_usrmclk = False,
                debug_signals        = self.host_spi_dq_debug,
                clk_freq             = sys_clk_freq,
                endianness           = self.cpu.endianness,
                adr_offset           = self.mem_map.get("hostspiflash", None))
            self.add_csr("hostspiflash")
            hostspiflash_size        = 64*mB
            hostspiflash_region      = SoCRegion(origin=self.mem_map.get("hostspiflash", None), size=hostspiflash_size, cached=False)
            self.bus.add_slave(name="hostspiflash", slave=self.hostspiflash.bus, region=hostspiflash_region)
            hostspiflashcfg_size     = 128
            hostspiflashcfg_region   = SoCRegion(origin=self.mem_map.get("hostspiflashcfg", None), size=hostspiflashcfg_size, cached=False)
            self.bus.add_slave(name="hostspiflashcfg", slave=self.hostspiflash.cfg_bus, region=hostspiflashcfg_region)

        # Host LPC Slave (Aquila core) -------------------------------------------------------------
        if with_hostlpcslave:
            hostlpcslave_pads = platform.request("hostlpcslave")
            self.host_lpc_debug = [Signal(), Signal(), Signal(), Signal(), Signal(), Signal(), Signal(), Signal()]
            self.host_lpc_clock_mirror = Signal()
            self.submodules.hostlpcslave   = AquilaLPCSlave(
                platform       = platform,
                pads           = hostlpcslave_pads,
                debug_signals  = self.host_lpc_debug,
                lpc_clk_mirror = self.host_lpc_clock_mirror,
                endianness     = self.cpu.endianness,
                adr_offset     = self.mem_map.get("hostlpcslave", None))
            self.add_csr("hostlpcslave")
            hostlpcslave_size   = 16*mB
            hostlpcslave_region = SoCRegion(origin=self.mem_map.get("hostlpcslave", None), size=hostlpcslave_size, cached=False)
            self.bus.add_slave(name="hostlpcslave", slave=self.hostlpcslave.slave_bus, region=hostlpcslave_region)
            self.bus.add_master(name="hostlpcslave", master=self.hostlpcslave.master_bus)

        # OpenFSI Master ---------------------------------------------------------------------------
        if with_openfsi_master:
            openfsi_master_pads = platform.request("openfsi_master")
            self.submodules.openfsi_master = OpenFSIMaster(
                platform        = platform,
                pads            = openfsi_master_pads,
                endianness      = self.cpu.endianness)
            self.add_csr("openfsimaster")
            openfsi_master_size = 128
            openfsi_master_region = SoCRegion(origin=self.mem_map.get("openfsimaster", None), size=openfsi_master_size, cached=False)
            self.bus.add_slave(name="openfsimaster", slave=self.openfsi_master.slave_bus, region=openfsi_master_region)

        # Debug hookups...
        #self.comb += debug2_pads.led_15.eq(self.host_spi_dq_debug[5])
        #self.comb += debug2_pads.led_14.eq(self.host_spi_dq_debug[4])
        #self.comb += debug2_pads.led_13.eq(hostspiflash4x_pads.clk)
        #self.comb += debug2_pads.led_12.eq(hostspiflash4x_pads.cs_n)
        #self.comb += debug2_pads.led_11.eq(self.host_spi_dq_debug[3])
        #self.comb += debug2_pads.led_10.eq(self.host_spi_dq_debug[2])
        #self.comb += debug2_pads.led_9.eq(self.host_spi_dq_debug[1])
        #self.comb += debug2_pads.led_8.eq(self.host_spi_dq_debug[0])

        if with_hostlpcslave and debug2_pads is not None:
            self.comb += debug2_pads.led_15.eq(self.host_lpc_debug[7])
            self.comb += debug2_pads.led_14.eq(self.host_lpc_debug[6])
            self.comb += debug2_pads.led_13.eq(self.host_lpc_debug[5])
            self.comb += debug2_pads.led_12.eq(self.host_lpc_debug[4])
            self.comb += debug2_pads.led_11.eq(self.host_lpc_debug[3])
            self.comb += debug2_pads.led_10.eq(self.host_lpc_debug[2])
            self.comb += debug2_pads.led_9.eq(self.host_lpc_debug[1])
            self.comb += debug2_pads.led_8.eq(self.host_lpc_debug[0])
            self.comb += lpc_debug_mirror_clock_pad.eq(self.host_lpc_clock_mirror)

        # I2C Masters ------------------------------------------------------------------------------
        if with_i2c_masters:
            for i2c_master_id in range(0, 5):
                i2c_master_pads = platform.request("i2c_master", i2c_master_id, True)
                if not i2c_master_pads:
                    continue

                master_name = "i2cmaster%d" % i2c_master_id

                i2cmaster = OpenCoresI2CMaster(
                    platform        = platform,
                    pads            = i2c_master_pads,
                    clk_freq        = sys_clk_freq)
                setattr(self.submodules, master_name, i2cmaster)
                self.add_csr(master_name)
                i2cmaster_size = 32
                i2cmaster_region = SoCRegion(origin=self.mem_map.get(master_name, None), size=i2cmaster_size, cached=False)
                self.bus.add_slave(name=master_name, slave=i2cmaster.bus, region=i2cmaster_region)
            if platform.request("i2c_master", None, True):
                raise ValueError("Unhandled i2c_master found")

        # SimplePWM --------------------------------------------------------------------------------
        if with_simple_pwm:
            pwm_tach_pads = platform.request("pwm_tach_pads")
            self.submodules.simple_pwm = SimplePWMSlave(
                platform        = platform,
                pads            = pwm_tach_pads,
                endianness      = self.cpu.endianness,
                pwm_clk_src     = 'sys',
                pwm_clk_freq    = sys_clk_freq)
            self.add_csr("simplepwm")
            simple_pwm_size = 128
            simple_pwm_region = SoCRegion(origin=self.mem_map.get("simplepwm", None), size=simple_pwm_size, cached=False)
            self.bus.add_slave(name="simplepwm", slave=self.simple_pwm.slave_bus, region=simple_pwm_region)

        # SimpleRTC --------------------------------------------------------------------------------
        if with_simple_rtc:
            self.submodules.simple_rtc = SimpleRTCSlave(
                platform        = platform,
                endianness      = self.cpu.endianness,
                rtc_clk_src     = 'sys',
                rtc_clk_freq    = sys_clk_freq)
            self.add_csr("simplertc")
            simple_rtc_size = 128
            simple_rtc_region = SoCRegion(origin=self.mem_map.get("simplertc", None), size=simple_rtc_size, cached=False)
            self.bus.add_slave(name="simplertc", slave=self.simple_rtc.slave_bus, region=simple_rtc_region)

        # Discrete LEDs ----------------------------------------------------------------------------
        from litex.soc.cores.gpio import GPIOTristate

        self.submodules.gpio1 = GPIOTristate(
            pads         = platform.request("user_leds"))
        self.add_csr("gpio1")

        # DIP switches -------------------------------------------------------------------------------------
        from litex.soc.cores.gpio import GPIOIn

        self.submodules.gpio2 = GPIOIn(
            pads         = Cat(*[platform.request("user_dip_btn", i) for i in range(8)]),
            with_irq     = True)
        self.add_csr("gpio2")

        # Alphanumeric display -----------------------------------------------------------------------------
        from litex.soc.cores.gpio import GPIOTristate

        self.submodules.gpio3 = GPIOTristate(
            pads         = platform.request("alpha_leds"))
        self.add_csr("gpio3")

    def set_gateware_dir(self, gateware_dir):
        self.gateware_dir = gateware_dir

    def initialize_rom(self, data):
        # Save actual expected contents for future use as gateware/rom.init
        content = ""
        formatter = "{:0" + str(int(self.rom.mem.width / 4)) + "X}\n"
        for d in data:
            content += formatter.format(d).zfill(int(self.rom.mem.width / 4))
        romfile = os.open(os.path.join(self.gateware_dir, "rom_data.init"), os.O_WRONLY | os.O_CREAT)
        os.write(romfile, content.encode())
        os.close(romfile)

        # Generate initial data to allow ecpbram to later stuff the bitstream
        (_, path) = tempfile.mkstemp()
        subprocess.check_call(["ecpbram", "-g", path, "-w", str(self.rom.mem.width), "-d", str(int(self.integrated_rom_size / 4)), "-s" "0"])

        # Convert data to binary
        random_file = open(path, 'r')
        data = []
        random_lines = random_file.readlines()
        for line in random_lines:
            data.append(int(line, 16))

        os.remove(path)

        self.rom.mem.init = data
        self.rom.mem.name_override = "rom"

# Build --------------------------------------------------------------------------------------------

def main():
    parser = argparse.ArgumentParser(description="LiteX SoC on Versa ECP5")
    parser.add_argument("--build",           action="store_true",              help="Build bitstream")
    parser.add_argument("--load",            action="store_true",              help="Load bitstream")
    parser.add_argument("--toolchain",       default="trellis",                help="FPGA toolchain: trellis (default) or diamond")
    parser.add_argument("--sys-clk-freq",    default=50e6,                     help="System clock frequency (default: 50MHz)")
    parser.add_argument("--device",          default="LFE5UM5G",               help="FPGA device (LFE5UM5G (default) or LFE5UM)")
    ethopts = parser.add_mutually_exclusive_group()
    ethopts.add_argument("--with-ethernet",  action="store_true",              help="Enable Ethernet support")
    ethopts.add_argument("--with-etherbone", action="store_true",              help="Enable Etherbone support")
    parser.add_argument("--eth-ip",          default="192.168.1.50", type=str, help="Ethernet/Etherbone IP address")
    parser.add_argument("--eth-phy",         default=0, type=int,              help="Ethernet PHY: 0 (default) or 1")
    builder_args(parser)
    soc_sdram_args(parser)
    trellis_args(parser)
    args = parser.parse_args()

    soc = BaseSoC(
        sys_clk_freq   = int(float(args.sys_clk_freq)),
        device         = args.device,
        with_ethernet  = args.with_ethernet,
        with_etherbone = args.with_etherbone,
        with_bmcspiflash = True,
        with_hostspiflash = True,
        with_hostlpcslave = True,
        with_openfsi_master = True,
        with_i2c_masters = True,
        with_simple_pwm = True,
        with_simple_rtc = True,
        eth_ip         = args.eth_ip,
        eth_phy        = args.eth_phy,
        toolchain      = args.toolchain,
        **soc_sdram_argdict(args)
    )
    builder = Builder(soc, **builder_argdict(args))

    # DRAM controller setup
    if production_mode:
        soc.add_constant("SDRAM_TEST_DISABLE")

    # Flash boot setup
    flash_boot_adr = soc.mem_map["bmcspiflash"] + 0x800000
    if production_mode:
        soc.add_constant("FLASH_BOOT_ADDRESS", flash_boot_adr)

    soc.set_gateware_dir(builder.gateware_dir)

    builder_kargs = trellis_argdict(args) if args.toolchain == "trellis" else {}
    builder.build(**builder_kargs, run=args.build)

    # Stuff the original rom into the fpga
    subprocess.check_call(["ecpbram",
                        "-i", os.path.join(builder.gateware_dir, soc.platform.name + ".config"),
                        "-o", os.path.join(builder.gateware_dir, soc.platform.name + "_stuffed.config"),
                        "-f", os.path.join(builder.gateware_dir, "rom.init"),
                        "-t", os.path.join(builder.gateware_dir, "rom_data.init")])

    # Update the svf / bit files
    subprocess.check_call(["ecppack",
                        os.path.join(builder.gateware_dir, soc.platform.name + "_stuffed.config"),
                        "--svf", os.path.join(builder.gateware_dir, soc.platform.name + ".svf"),
                        "--bit", os.path.join(builder.gateware_dir, soc.platform.name + ".bit"),
                        "--spimode", "fast-read",
                        "--freq", "38.8",
                        "--compress",
                        "--bootaddr", "0"])

    if args.load:
        prog = soc.platform.create_programmer()
        prog.load_bitstream(os.path.join(builder.gateware_dir, soc.build_name + ".svf"))

if __name__ == "__main__":
    main()
